upstream backend {server backend:80;}

map $http_upgrade $connection_upgrade {default upgrade;'' close;}
underscores_in_headers on;

server {

    listen 80;
    server_name httpbin-aws.ddns.net;

    if ($scheme != "https") {
        return 301 https://$host$request_uri;
    }

}

server {

    listen 443 ssl http2;
    server_name httpbin-aws.ddns.net;

    ssl_certificate /etc/letsencrypt/live/httpbin-aws.ddns.net/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/httpbin-aws.ddns.net/privkey.pem;
#    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

    ssl_stapling               on;
    ssl_stapling_verify        on;
    resolver                   8.8.8.8;
    ssl_prefer_server_ciphers  on;
    ssl_session_timeout        24h;
    ssl_session_cache          shared:SSL:2m;
    ssl_session_tickets        off;

    ssl_protocols              TLSv1.2 TLSv1.3;
    ssl_ciphers                EECDH+AESGCM:EDH+AESGCM;
    ssl_ecdh_curve             secp384r1;
    add_header                 Content-Security-Policy-Report-Only "default-src https:; script-src https: 'unsafe-eval' 'unsafe-inline'; style-src https: 'unsafe-inline'; img-src https: data:; font-src https: data:; report-uri /csp-report";
    add_header                 Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
    add_header                 X-Frame-Options DENY;
    add_header                 X-Content-Type-Options nosniff;
    add_header                 X-XSS-Protection "1; mode=block";

    location / {

        proxy_buffers         8 16k;
        proxy_buffer_size     16k;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;

        proxy_cache_bypass $http_upgrade;

        proxy_read_timeout 30;
        proxy_pass_request_headers on;
        proxy_pass http://backend;

    }
}
